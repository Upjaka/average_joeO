package ru.spbstu.competition.game

import ru.spbstu.competition.protocol.data.Claim
import ru.spbstu.competition.protocol.data.River
import ru.spbstu.competition.protocol.data.Setup
import java.util.*

enum class RiverState{ Our, Enemy, Neutral }

class State {
    val rivers = mutableMapOf<River, RiverState>()
    var mines = listOf<Int>()
    var myId = -1

    fun init(setup: Setup) {
        myId = setup.punter
        for(river in setup.map.rivers) {
            rivers[river] = RiverState.Neutral
        }
        for(mine in setup.map.mines) {
            mines += mine
        }
    }

    fun update(claim: Claim) {
        rivers[River(claim.source, claim.target)] = when(claim.punter) {
            myId -> RiverState.Our
            else -> RiverState.Enemy
        }
    }
}

class Graph {
    private data class Site(val number: Int, val important: Boolean) {
        val neighbors = mutableSetOf<Site>()
    }

    private val sites = mutableListOf<Site>()
    private val rivers = mutableMapOf<River, RiverState>()

    fun init(setup: Setup) {
        for (river in setup.map.rivers) {
            rivers[river] = RiverState.Neutral
            if (river.source !in setup.map.mines) {
                this.addSite(river.source, false)
            } else {
                this.addSite(river.source, true)
            }

            if (river.target !in setup.map.mines) {
                this.addSite(river.target, false)
            } else {
                this.addSite(river.target, true)
            }

            this.connect(river.source, river.target)
        }
    }

    private fun connect(first: Site?, second: Site?) {
        if (second != null) first?.neighbors?.add(second)
        if (first != null) second?.neighbors?.add(first)

    }

    fun addSite(number: Int, isImportant: Boolean) {
        sites[number] = Site(number, isImportant)
    }

    fun addSites(numbers: Map<Int, Boolean>) {
        for (number in numbers.keys) {
            sites[number] = Site(number, numbers.getValue(number))
        }
    }

    fun connect(first: Int, second: Int) = connect(sites[first], sites[second])

    fun neighbors(number: Int): List<Int> =
            sites[number]?.neighbors?.map { it.number } ?: listOf()

    fun Graph.dfs(start: Int, finish: Int) = dfs(start, finish, setOf()) ?: -1

    fun Graph.dfs(start: Int, finish: Int, visited: Set<Int>): Int? =
            if (start == finish) 0
            else {
                val min = neighbors(start).filter {
                    it !in visited
                }.map {
                    dfs(it, finish, visited + start)
                }.filterNotNull().min()
                if (min == null) null else min + 1
            }

    fun Graph.bfs(start: Int, finish: Int): Int {
        val queue = ArrayDeque<Int>()
        queue.add(start)
        val visited = mutableMapOf(start to 0)
        while (queue.isNotEmpty()) {
            val next = queue.poll()
            val distance = visited[next]!!
            if (next == finish) return distance
            for (neighbor in neighbors(next)) {
                if (neighbor in visited) continue
                visited[neighbor] = distance + 1
                queue.add(neighbor)
            }
        }
        return -1
    }

    override fun toString(): String {
        var result = StringBuilder()
        for (site in sites) {
            result.append(site.number)
            result.append(" ")
            result.append(site.important)
            result.append("\n")
        }
        return result.toString()
    }
}
