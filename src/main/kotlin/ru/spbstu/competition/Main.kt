package ru.spbstu.competition

import org.kohsuke.args4j.CmdLineParser
import org.kohsuke.args4j.Option
import ru.spbstu.competition.game.Graph
import ru.spbstu.competition.game.Intellect
import ru.spbstu.competition.game.State
import ru.spbstu.competition.protocol.Protocol
import ru.spbstu.competition.protocol.data.*

object Arguments {
    @Option(name = "-u", usage = "Specify server url")
    var url: String = ""

    @Option(name = "-p", usage = "Specify server port")
    var port: Int = -1

    fun use(args: Array<String>): Arguments =
            CmdLineParser(this).parseArgument(*args).let{ this }
}

fun main(args: Array<String>) {
    Arguments.use(args)

    println("Hi, I am Steve")

    // Протокол обмена с сервером
    val protocol = Protocol(Arguments.url, Arguments.port)
    // Состояние игрового поля
    val gameState = State()
    // Ум нашего бота
    val intellect = Intellect(gameState, protocol)
    // Граф состояния игрового поля
    val gameGraph = Graph()

    protocol.handShake("Bot Steve")
    val setupData = protocol.setup()
    gameState.init(setupData)
    gameGraph.init(setupData)

    println("Received id = ${setupData.punter}")

    println(gameGraph.toString())

    protocol.ready()

    gameloop@ while(true) {
        val message = protocol.serverMessage()
        when(message) {
            is GameResult -> {
                println("The game is over!")
                val myScore = message.stop.scores[protocol.myId]
                println("Steve scored ${myScore.score} points!")
                break@gameloop
            }
            is Timeout -> {
                println("Steve too slow =(")
            }
            is GameTurnMessage -> {
                for(move in message.move.moves) {
                    when(move) {
                        is PassMove -> {}
                        is ClaimMove -> gameState.update(move.claim)
                    }
                }
            }
        }

        println("Steve thinking")
        intellect.makeMove()
        println("Steve made move!")
    }
}
